<?php

use App\Http\Controllers\Api\Customer\OrderController;
use App\Http\Controllers\Api\staff\OrderController as StaffOrderController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\Staff\ServiceController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('v1')->group(function () {
    // Register and login routes
    Route::get('register', [UserController::class, 'register'])->name('register');
    Route::get('login', [UserController::class, 'login'])->name('login');

    // Customer route
    Route::group(['prefix' => 'customer', 'as' => 'customer.'], static function () {
        Route::middleware(['auth:api', 'customer'])->group(function () {
            Route::get('show-order', [OrderController::class, 'showOrder'])->name('show-order');
            Route::post('create-order', [OrderController::class, 'store'])->name('store-order');
            Route::patch('cancel-order', [OrderController::class, 'cancel'])->name('cancel-order');
        });
    });

    // Staff route
    Route::group(['prefix' => 'staff', 'as' => 'staff.'], static function () {
        Route::middleware(['auth:api', 'staff'])->group(function () {
            // Service
            Route::get('show-all-service', [ServiceController::class, 'showAll'])->name('show-all-service');
            Route::post('create-service', [ServiceController::class, 'store'])->name('store-service');
            Route::patch('update-service', [ServiceController::class, 'edit'])->name('update-service');
            Route::delete('delete-service', [ServiceController::class, 'delete'])->name('delete-service');

            // Order
            Route::get('show-order', [StaffOrderController::class, 'showOrder'])->name('show-order');
            Route::patch('done-order', [StaffOrderController::class, 'done'])->name('done-order');
        });
    });
});
