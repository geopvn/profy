## How to install project
1. Open terminal and run this code
```
composer install
```
2. In this project you can find .env file, where you can change database credential
```
DB_CONNECTION=mysql <-- The project uses mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=profy
DB_USERNAME=root
DB_PASSWORD=
```
3. Open terminal and run this code
```
3.1 php artisan migrate
3.2 php artisan passport:install
```

## Run the project
The postman collection build on 8888 port
```
php artisan serv --port 8888
```

## Postman collection link
```
https://www.getpostman.com/collections/5dc76678745f1c95d287
```

