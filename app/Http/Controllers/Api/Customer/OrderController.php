<?php

namespace App\Http\Controllers\Api\Customer;

use App\Http\Controllers\Controller;
use App\Http\Requests\Customer\CancelOrderRequest;
use App\Http\Requests\Customer\CreateOrderRequest;
use App\Http\Requests\Customer\ShowOrderRequest;
use App\Repositories\Customer\OrderRepository;

class OrderController extends Controller
{
    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function showOrder(ShowOrderRequest $request): object
    {
        $validatedData = $request->validated();
        $order = $this->orderRepository->getAllBy($validatedData);

        if ((array)$order) {
            return response()->json([
                'status' => 'success',
                'result' => $order
            ], 200);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'something wrong'
        ], 422);
    }

    public function store(CreateOrderRequest $request): object
    {
        $validatedData = $request->validated();
        $order = $this->orderRepository->create($validatedData);

        if ($order->status !== 'error') {
            return response()->json([
                'status' => 'success',
                'result' => $order
            ], 200);
        }

        return response()->json($order, 422);
    }

    public function cancel(CancelOrderRequest $request): object
    {
        $validatedData = $request->validated();
        $order = $this->orderRepository->cancel($validatedData);

        if ($order->status !== 'error') {
            return response()->json($order, 200);
        }

        return response()->json($order, 422);
    }
}
