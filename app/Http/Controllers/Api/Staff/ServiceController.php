<?php

namespace App\Http\Controllers\Api\Staff;

use App\Http\Controllers\Controller;
use App\Http\Requests\Staff\CreateServiceRequest;
use App\Http\Requests\Staff\DeleteServiceRequest;
use App\Http\Requests\Staff\UpdateServiceRequest;
use App\Repositories\Staff\ServiceRepository;

class ServiceController extends Controller
{
    private $serviceRepository;

    public function __construct(ServiceRepository $serviceRepository)
    {
        $this->serviceRepository = $serviceRepository;
    }

    public function showAll(): object
    {
        $service = $this->serviceRepository->getAll();

        if ((array)$service) {
            return response()->json([
                'status' => 'success',
                'result' => $service
            ], 200);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'something wrong'
        ], 422);
    }

    public function store(CreateServiceRequest $request): object
    {
        $validatedData = $request->validated();
        $service = $this->serviceRepository->create($validatedData);

        if ((array)$service) {
            return response()->json([
                'status' => 'success',
                'result' => $service
            ], 200);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'something wrong'
        ], 422);
    }

    public function edit(UpdateServiceRequest $request): object
    {
        $validatedData = $request->validated();
        $service = $this->serviceRepository->update($validatedData);

        if ((array)$service) {
            return response()->json([
                'status' => 'success',
                'result' => $service
            ], 200);
        }

        return response()->json([
            'status' => 'error',
            'message' => 'send data is invalid'
        ], 422);
    }

    public function delete(DeleteServiceRequest $request): object
    {
        $validatedData = $request->validated();
        $service = $this->serviceRepository->remove($validatedData);

        if ($service) {
            return response()->json([
                'status' => 'success',
                'result' => $service
            ], 200);
        }

        return response()->json([
            'status' => 'error',
            'message' => 'send data is invalid'
        ], 422);
    }
}
