<?php

namespace App\Http\Controllers\Api\Staff;

use App\Http\Controllers\Controller;
use App\Http\Requests\Staff\DoneOrderRequest;
use App\Http\Requests\Staff\ShowOrderRequest;
use App\Repositories\Staff\OrderRepository;

class OrderController extends Controller
{
    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function showOrder(ShowOrderRequest $request): object
    {
        $validatedData = $request->validated();
        $order = $this->orderRepository->getAllBy($validatedData);

        if ((array)$order) {
            return response()->json([
                'status' => 'success',
                'result' => $order
            ], 200);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'something wrong'
        ], 422);
    }

    public function done(DoneOrderRequest $request): object
    {
        $validatedData = $request->validated();
        $order = $this->orderRepository->done($validatedData);

        if ($order->status !== 'error') {
            return response()->json($order, 200);
        }

        return response()->json($order, 422);
    }
}
