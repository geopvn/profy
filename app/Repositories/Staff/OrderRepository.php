<?php

namespace App\Repositories\Staff;


use App\Model\Order;

class OrderRepository
{
    public function getAllBy($data): object
    {
        if ($data["status"] === 'all') {
            return Order::query()->where('staff_id', '=', auth()->id())->get();
        }
        return Order::query()->where([
                    ['user_id', '=', auth()->id()],
                    ['status', '=', $data["status"]],
                ])->get();
    }

    public function done($data): object
    {
        $result = Order::query()->where([
                        ['staff_id', '=', auth()->id()],
                        ['status', '=', 'progress']
                    ])->find($data["order_id"]);
        $update = ['status' => 'done'];
        $update_price = array_get($data, 'update_price');
        if ($update_price !== null) {
            $update['update_price'] = parse_to_money($update_price);
        }

        if ($result !== null) {
            $result->update($update);
            return (object) [
                'status' => 'success',
                'message' => 'The order was successfully done'
            ];
        }

        return (object) [
            'status' => 'error',
            'message' => 'Order not found'
        ];
    }
}
