<?php

namespace App\Repositories\Staff;


use App\Model\Service;

class ServiceRepository
{
    public function getAll(): object
    {
        return Service::query()->where('user_id', '=', auth()->id())->get();
    }

    public function create($data): object
    {
        $data["user_id"] = auth()->id();
        $data["price"] = parse_to_money($data["price"]);
        return Service::query()->create($data);
    }

    public function update($data): object
    {
        $data["price"] = parse_to_money($data["price"]);
        $result = Service::query()->where('user_id', '=', auth()->id())->find($data["id"]);

        if ($result !== null) {
            $result->update($data);
            return $result;
        }

        return (object) [];
    }

    public function remove($data): bool
    {
        $service = Service::query()->where('user_id', '=', auth()->id())->find($data["id"]);
        if ($service !== null) {
            $service->delete();
            return true;
        }
        return false;
    }
}
