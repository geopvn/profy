<?php

namespace App\Repositories\Customer;


use App\Model\Order;
use App\Model\Service;

class OrderRepository
{
    public function getAllBy($data): object
    {
        if ($data["status"] === 'all') {
            return Order::query()->where('user_id', '=', auth()->id())->get();
        }
        return Order::query()->where([
                    ['user_id', '=', auth()->id()],
                    ['status', '=', $data["status"]],
                ])->get();
    }

    public function create($data): object
    {
        $service = Service::query()->find($data["service_id"]);
        if ($service !== null) {
            $order = Order::query()->where([
                ['user_id', '=', auth()->id()],
                ['service_id', '=', $data["service_id"]],
                ['status', '=', 'progress'],
            ])->first();
            if ($order === null) {
                $data["user_id"] = auth()->id();
                $data["staff_id"] = $service->user_id;
                $data["price"] = $service->price;
                $data["status"] = 'progress';
                return Order::query()->create($data);
            }
            return (object) [
                'status' => 'error',
                'message' => 'You already have such an order'
            ];
        }

        return (object) [
            'status' => 'error',
            'message' => 'Service not found'
        ];
    }

    public function cancel($data): object
    {
        $result = Order::query()->where('user_id', '=', auth()->id())->find($data["order_id"]);
        $update = ['status' => 'cancel'];
        if ($result !== null) {
            $result->update($update);
            return (object) [
                'status' => 'success',
                'message' => 'The order was successfully canceled'
            ];
        }

        return (object) [
            'status' => 'error',
            'message' => 'Order not found'
        ];
    }
}
