<?php

use Illuminate\Support\Arr;

if (!function_exists('parse_to_money')) {
    function parse_to_money($money)
    {
        return intval($money * 100);
    }
}

if (!function_exists('money_to_human')) {
    function money_to_human($money)
    {
        return number_format($money / 100, 2);
    }
}

if (! function_exists('array_get')) {
    /**
     * Get an item from an array using "dot" notation.
     *
     * @param  \ArrayAccess|array  $array
     * @param  string  $key
     * @param  mixed   $default
     * @return mixed
     */
    function array_get($array, $key, $default = null)
    {
        return Arr::get($array, $key, $default);
    }
}
